import { User } from './answer';

describe('User', () => {
  it('should create a user with valid input', () => {
    const input = { name: 'John Doe', email: 'john.doe@example.com', password: 'Password123', age: 25 };
    const user = User.create(input);

    expect(user).toBeInstanceOf(User);
  });

  it('should not create a user with a name shorter than 7 characters', () => {
    const input = { name: 'John', email: 'john.doe@example.com', password: 'Password123', age: 25 };
    const result = User.create(input);

    expect(result).toBeInstanceOf(Error);
    if (result instanceof Error) {
      expect(result.message).toBe("Name must be longer than 6 characters");
    }
  });

  it('should not create a user with an invalid email', () => {
    const input = { name: 'Johnathan Doe', email: 'john.doe', password: 'Password123', age: 25 };
    const result = User.create(input);
  
    expect(result).toBeInstanceOf(Error);
    if (result instanceof Error) {
      expect(result.message).toBe("Email must be a valid email");
    }
  });
  
  it('should not create a user with a password shorter than 9 characters', () => {
    const input = { name: 'Johnathan Doe', email: 'john.doe@example.com', password: 'Pass123', age: 25 };
    const result = User.create(input);
  
    expect(result).toBeInstanceOf(Error);
    if (result instanceof Error) {
      expect(result.message).toBe("Password must be longer than 8 characters and contain at least one letter and one number");
    }
  });
  
  it('should not create a user with a password without digits', () => {
    const input = { name: 'Johnathan Doe', email: 'john.doe@example.com', password: 'Password!', age: 25 };
    const result = User.create(input);
  
    expect(result).toBeInstanceOf(Error);
    if (result instanceof Error) {
      expect(result.message).toBe("Password must be longer than 8 characters and contain at least one letter and one number");
    }
  });
  
  it('should not create a user with a password without letters', () => {
    const input = { name: 'Johnathan Doe', email: 'john.doe@example.com', password: '123456789!', age: 25 };
    const result = User.create(input);
  
    expect(result).toBeInstanceOf(Error);
    if (result instanceof Error) {
      expect(result.message).toBe("Password must be longer than 8 characters and contain at least one letter and one number");
    }
  });
  
  it('should not create a user with a negative age', () => {
    const input = { name: 'Johnathan Doe', email: 'john.doe@example.com', password: 'Password123', age: -1 };
    const result = User.create(input);
  
    expect(result).toBeInstanceOf(Error);
    if (result instanceof Error) {
      expect(result.message).toBe("Age must be a positive integer");
    }
  });
  
  it('should not create a user with a non-integer age', () => {
    const input = { name: 'Johnathan Doe', email: 'john.doe@example.com', password: 'Password123', age: 25.5 };
    const result = User.create(input);
  
    expect(result).toBeInstanceOf(Error);
    if (result instanceof Error) {
      expect(result.message).toBe("Age must be a positive integer");
    }
  });
});
